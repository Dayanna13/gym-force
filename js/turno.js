function validar() {
    var nombre, edad, deporte,telefono, expresion;
    nombre = document.getElementById("nombre").value;
    edad = document.getElementById("edad").value;
    deporte = document.getElementById("deporte").value;
    correo = document.getElementById("correo").value;
    telefono = document.getElementById("telefono").value;

    expresion = /\w+@\w+\.+[a-z]/;

    if(nombre == "" || edad == "" || deporte == "" || telefono == "" ){
        alert("Campos obligatorios");
        return false;
    } else if(!expresion.test(correo)){
        alert("Correo no valido");
        return false;
    } else {
        alert("GYM FORCE - TURNO DEL DEPORTE REGISTRADO CORRECTAMENTE");
        window.location = "indexUsuario.html";
        return false;
    }
}