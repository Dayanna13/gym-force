function iniciar(){
    var usuario, clave;
    usuario = document.getElementById("usuario").value;
    clave = document.getElementById("clave").value;

    if(usuario == "" || clave == ""){
        alert("Rellene los campos");
        return false;
    } else if(usuario == "administrador" && clave == "admin123"){
        alert("Bienvenido Administrador a GYM FORCE");
        window.location = "indexadministrador.html";
        return false;
    } else if(usuario == "usuario" && clave == "usuario123"){
        alert("GYM FORCE te da una cordial bienvenida");
        window.location = "indexUsuario.html";
        return false;
    } else {
        alert("Datos incorrectos, verifique usuario y contraseña");
        return false;
    }
}
